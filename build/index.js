"use strict";

var _Hello = _interopRequireDefault(require("./Hello"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_Hello.default.print();

new _Hello.default("All").print();